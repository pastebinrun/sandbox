# WARNING: This doesn't have the sandbox, it's supposed to be used for
# CI testing only.
FROM golang:1.13.8 AS builder
WORKDIR /runner
COPY go.mod go.sum runner.go /runner/
RUN go build

FROM fedora:31
# Manage list of packages in a single place
COPY roles/packages /ansible/roles/packages
WORKDIR /ansible
RUN dnf install -y ansible && ansible localhost -m include_role -a name=packages && dnf remove -y ansible && dnf clean all && rm -rf /var/cache/yum
RUN rm -r /ansible
COPY --from=builder runner/sandbox /sandbox
ENV UNSAFE_DISABLE_SANDBOX=
ENTRYPOINT /sandbox
EXPOSE 8888
