package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/sync/errgroup"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"time"
)

type httpError struct {
	description string
	statusCode  int
}

type form struct {
	Files []struct {
		Name     string
		Contents string
	}
	Stdin string
	Code  string
}

func handler(w http.ResponseWriter, body io.Reader) *httpError {
	form, err := decodeJson(body)
	if err != nil {
		return &httpError{err.Error(), http.StatusBadRequest}
	}
	out, err := runInSandbox(form)
	if err != nil {
		return &httpError{err.Error(), http.StatusInternalServerError}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(out)
	return nil
}

func decodeJson(body io.Reader) (*form, error) {
	d := json.NewDecoder(body)
	d.DisallowUnknownFields()
	var f form
	err := d.Decode(&f)
	if err != nil {
		return nil, err
	}
	if d.More() {
		return nil, errors.New("extraneous data after JSON object")
	}
	return &f, nil
}

type output struct {
	Status *int   `json:"status"`
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr"`
}

func runInSandbox(form *form) (*output, error) {
	home, err := ioutil.TempDir("", "runner-home")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(home)
	for _, file := range form.Files {
		f, err := os.Create(path.Join(home, path.Base(file.Name)))
		if err != nil {
			return nil, err
		}
		_, err = io.WriteString(f, file.Contents)
		if err != nil {
			return nil, err
		}
		err = f.Close()
		if err != nil {
			return nil, err
		}
	}
	var running *exec.Cmd
	if _, exists := os.LookupEnv("UNSAFE_DISABLE_SANDBOX"); exists {
		running = exec.Command("sh", "-c", form.Code)
		running.Dir = home
	} else {
		running = exec.Command(
			"firejail",
			"--caps.drop=all",
			"--net=none",
			"--no3d",
			"--nodbus",
			"--nodvd",
			"--nogroups",
			"--nonewprivs",
			"--noprofile",
			"--noroot",
			"--nosound",
			"--noautopulse",
			"--novideo",
			"--nou2f",
			"--private="+home,
			"--private-dev",
			"--private-tmp",
			"--rlimit-cpu=10",
			"--rlimit-fsize=10000000",
			"--rlimit-nofile=192",
			"--rlimit-nproc=1024",
			"--seccomp",
			"--shell=none",
			"--x11=none",
			"--quiet",
			"sh",
			"-c",
			form.Code,
		)
	}
	stdin, err := running.StdinPipe()
	if err != nil {
		return nil, err
	}
	stdout, err := running.StdoutPipe()
	if err != nil {
		return nil, err
	}
	stderr, err := running.StderrPipe()
	if err != nil {
		return nil, err
	}
	if err := running.Start(); err != nil {
		return nil, err
	}
	finished := make(chan struct{}, 1)
	go func() {
		select {
		case <-time.After(30 * time.Second):
			err := exec.Command("firejail", fmt.Sprintf("--shutdown=%d", running.Process.Pid)).Run()
			if err != nil {
				panic(err)
			}
		case <-finished:
		}
	}()
	var output output
	var g errgroup.Group
	g.Go(func() error {
		bytes, err := ioutil.ReadAll(io.LimitReader(stdout, 1000000))
		if err != nil {
			return err
		}
		if err := stdout.Close(); err != nil {
			return err
		}
		output.Stdout = string(bytes)
		return nil
	})
	g.Go(func() error {
		bytes, err := ioutil.ReadAll(io.LimitReader(stderr, 1000000))
		if err != nil {
			return err
		}
		if err := stderr.Close(); err != nil {
			return err
		}
		output.Stderr = string(bytes)
		return nil
	})
	g.Go(func() error {
		_, err := io.WriteString(stdin, form.Stdin)
		if err != nil {
			return err
		}
		err = stdin.Close()
		if err != nil {
			return err
		}
		return nil
	})
	if err := g.Wait(); err != nil {
		return nil, err
	}
	running.Wait()
	finished <- struct{}{}
	code := running.ProcessState.ExitCode()
	if code != -1 {
		output.Status = &code
	}
	return &output, nil
}

func wrap(f func(w http.ResponseWriter, body io.Reader) *httpError) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		httpErr := f(w, req.Body)
		if httpErr != nil {
			http.Error(w, httpErr.description, httpErr.statusCode)
		}
	}
}

func main() {
	home, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	os.Chdir(home)
	http.HandleFunc("/", wrap(handler))
	log.Fatal(http.ListenAndServe(":8888", nil))
}
